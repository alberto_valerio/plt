package plt;

import static org.junit.Assert.*;

import org.junit.Test;

public class TranslatorTest {
	
	
	@Test
	public void testInputPhrase() {
		String inputPhrase = "Hello world!";
		Translator translator = new Translator(inputPhrase);
		assertEquals("Hello world!", translator.getPhrase());
	}

	@Test
	public void testTranslationEmptyPhrase() throws Exception {
		String inputPhrase = "";
		Translator translator = new Translator(inputPhrase);
		assertEquals(Translator.NIL, translator.translate());
	}

	@Test
	public void testTranslationPhraseStartingWithAEndingWithY() throws Exception {
		String inputPhrase = "any";
		Translator translator = new Translator(inputPhrase);
		assertEquals("anynay", translator.translate());
	}

	@Test
	public void testTranslationPhraseStartingWithUEndingWithY() throws Exception {
		String inputPhrase = "utility";
		Translator translator = new Translator(inputPhrase);
		assertEquals("utilitynay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithVowel() throws Exception {
		String inputPhrase = "apple";
		Translator translator = new Translator(inputPhrase);
		assertEquals("appleyay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseEndingWithConsonant() throws Exception {
		String inputPhrase = "ask";
		Translator translator = new Translator(inputPhrase);
		assertEquals("askay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithSingleConsonant() throws Exception {
		String inputPhrase = "hello";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithTwoConsonants() throws Exception {
		String inputPhrase = "known";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ownknay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithMoreThanTwoConsonants() throws Exception {
		String inputPhrase = "scream";
		Translator translator = new Translator(inputPhrase);
		assertEquals("eamscray", translator.translate());
	}

	@Test
	public void testTranslationPhraseContainigMoreWordsSepBySpases() throws Exception {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway", translator.translate());
	}

	@Test
	public void testTranslationPhraseContainigMoreWordsSepByDashes() throws Exception {
		String inputPhrase = "well-being";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellway-eingbay", translator.translate());
	}

	@Test
	public void testTranslationPhraseContainigOnePunctuationMark() throws Exception {
		String inputPhrase = "hello world!";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway!", translator.translate());
	}

	@Test
	public void testTranslationPhraseContainigMorePunctuationMarks() throws Exception {
		String inputPhrase = "hello (world)!";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay (orldway)!", translator.translate());
	}

	@Test
	public void testCaseSensitiveTranslationUppercase() throws Exception {
		String inputPhrase = "APPLE";
		Translator translator = new Translator(inputPhrase);
		assertEquals("APPLEYAY", translator.translate());
	}

	@Test
	public void testCaseSensitiveTranslationTitlecase() throws Exception {
		String inputPhrase = "Hello";
		Translator translator = new Translator(inputPhrase);
		assertEquals("Ellohay", translator.translate());
	}

	@Test (expected = PigLatinException.class)
	public void testCaseSensitiveTranslationCaseNotLegit() throws Exception {
		String inputPhrase = "biRd";
		Translator translator = new Translator(inputPhrase);
		translator.translate();
	}

	

}
