package plt;

public class Translator {
	
	public static final String NIL = "nil";
	public static final String UPPERCASE = "UPPER CASE";
	public static final String TITLECASE = "TITLE CASE";
	public static final String LOWERCASE = "LOWER CASE";
	protected static final char[] VOWELS = {'a','e','i','o','u','A','E','I','O','U'};
	protected static final char[] SPECIAL_CHARS = {' ', '-', '.', ',', ';', ':', '?', '!', '\'', '(', ')'};
	
	private String phrase;
	private StringBuilder word = new StringBuilder();
	private StringBuilder phraseTranslated = new StringBuilder();

	public Translator(String inputPhrase) {
		phrase = inputPhrase;
	}

	public String getPhrase() {
		return phrase;
	}

	public String translate() throws Exception {
		if(phrase.isEmpty()) {
			return NIL;
		} else {
			
			for (int i = 0; i < phrase.length(); i++) {	
				specialCharParser(phrase.charAt(i),i);
			}

			return phraseTranslated.toString();
		}
	}
	
	private boolean startWithVowel(String word) {
		char firstLetter = word.charAt(0);
		return new String(VOWELS).indexOf(firstLetter) != -1;
	}

	private boolean endWithVowel(String word) {
		char lastLetter = word.charAt(word.length() - 1);
		return new String(VOWELS).indexOf(lastLetter) != -1;
	}
	
	private boolean isVowel(char c) {
		return new String(VOWELS).indexOf(c) != -1;
	}

	private boolean isSpecialChar(char c) {
		return new String(SPECIAL_CHARS).indexOf(c) != -1;
	}
	
	private String checkCase(String word) throws PigLatinException {
		int upperCaseCounter = 0;
		String wordCase = "";
		for (int i = 0; i < word.length(); i++) {
			if(Character.isUpperCase(word.charAt(i))) {
				upperCaseCounter++;
			}
		}
		if(word.length() == upperCaseCounter) {
			wordCase = UPPERCASE;
		} else if(Character.isUpperCase(word.charAt(0)) && upperCaseCounter == 1) {
			wordCase = TITLECASE;
		} else if (upperCaseCounter == 0) {
			wordCase = LOWERCASE;
		} else {
			throw new PigLatinException("Case not managed!");
		}
		return wordCase;
	}
	
	private String caseCorrector(String word, String finalCase) {
		String wordCorrected = "";
		if(finalCase.equals(TITLECASE)) {
			word = word.toLowerCase();
			wordCorrected = word.substring(0, 1).toUpperCase() + word.substring(1);
		} else if(finalCase.equals(UPPERCASE)) {
			wordCorrected = word.toUpperCase();
		} else {
			wordCorrected = word.toLowerCase();
		}
		return wordCorrected;
	}
	
	private void translateWordStartingWithVowel(String word) throws Exception {
		String wordTranslated = "";
		if(word.endsWith("y")) {
			wordTranslated = word + "nay";
			phraseTranslated.append(  caseCorrector(wordTranslated, checkCase(word)) );
		} else if(endWithVowel(word)) {
			wordTranslated = word + "yay";
			phraseTranslated.append( caseCorrector(wordTranslated, checkCase(word)) );
		} else {
			wordTranslated = word + "ay";
			phraseTranslated.append( caseCorrector(wordTranslated, checkCase(word)) );
		}
	}

	private void translateWordStartingWithConsonant(String word) throws Exception {
		String wordTranslated = "";		
		if(isVowel(word.charAt(1))) {
			wordTranslated = word.substring(1) + word.charAt(0) + "ay";
			phraseTranslated.append( caseCorrector(wordTranslated, checkCase(word)) );
		} else {
			StringBuilder removedConsonants = new StringBuilder();
			removedConsonants.append("" + word.charAt(0) + word.charAt(1));
			int counter = 2;
			while( !isVowel(word.charAt(counter)) ) {
				removedConsonants.append(word.charAt(counter));
				counter++;
			}
			wordTranslated = word.substring(counter) + removedConsonants + "ay";
			phraseTranslated.append( caseCorrector(wordTranslated, checkCase(word)) );
		}
	}
	
	private void translateSingleWord(String word) throws Exception {
		if(startWithVowel(word)) {
			translateWordStartingWithVowel(word);
		} else {
			translateWordStartingWithConsonant(word);
		}
	}
	
	private void specialCharParser(char c, int charIndex) throws Exception {
		if(isSpecialChar(c) || charIndex == phrase.length() - 1) {

			if (charIndex == phrase.length() - 1 && !isSpecialChar(c)) {
				word.append("" + c);
				translateSingleWord(word.toString());
			} else if(word.length() > 0) {
				translateSingleWord(word.toString());
				phraseTranslated.append("" + c);
			} else {
				phraseTranslated.append("" + c);
			}
			word.setLength(0);
			
		} else {
			word.append("" + c);	
		}
	}

}
