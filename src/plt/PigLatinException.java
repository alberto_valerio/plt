package plt;

public class PigLatinException extends Exception {

	public PigLatinException(String message) {
		super(message);
	}
}
